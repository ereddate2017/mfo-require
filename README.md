# mfo-require

#### 介绍
mfo-require，前端模块加载

#### 全量demo
查看，[[index.html](https://gitee.com/ereddate2017/mfo-require/blob/master/public/index.html)]

#### 发行说明

Latest version: [![npm version](https://img.shields.io/npm/v/mfo-require/latest.svg)](https://www.npmjs.com/package/mfo-require)

[![](https://data.jsdelivr.com/v1/package/npm/mfo-require/badge)](https://www.jsdelivr.com/package/npm/mfo-require)
#### 使用

##### 兼容控制

如果你得使用的浏览器不支持 promise (比如 IE)，就在 head 中引入：

```
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.js">
</script>
```

或

```
npm install es6-promise --save

import 'es6-promise/auto'
```